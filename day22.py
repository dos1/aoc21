L,M=[(*[[*map(int,c.split('..'))]for c in l],z=="on")for z,l in[(z,[c.split('=')[1]for c in l.split(',')])for z,l in[l.strip().split(' ')for l in open("inputday22")]]],lambda a,b:b-a+1
C,D,I,R=lambda a,b,c,*_:[M(*a)*M(*b)*M(*c),0][a[0]>a[1]or b[0]>b[1]or c[0]>c[1]],lambda d:C(*d),lambda a,b:[(max(a[x][0],b[x][0]),min(a[x][1],b[x][1]))for x in[0,1,2]],[]
for l in L:i=[[*I(c,l),1-c[3]]for c in R if D(I(l,c))];R+=[i,[l,*i]][l[3]]
print(*(sum(D(t(c))*[-1,1][c[3]]for c in R)for t in[lambda x:I(x,[(-50,50)]*3),lambda x:x]))
